#___ Leitura dos bancos ----

#Salvando os objetos e funcoes criados para carregar dps
load("~/Estudos/PS Estat/PS dados.RData")

#Pacotes utilizados
library(pacman)
pacman::p_load(readr, tidyverse, lubridate, lattice, statsr,PMCMR, goftest, 
               DescTools, countrycode, scales)

#Leitura dos bancos
setwd("~/Estudos/PS Estat")
athlete_events <- read_csv("athlete_events.csv")
country_definitions <- read_delim("country_definitions.csv", 
                                  delim = ";", escape_double = FALSE, trim_ws = TRUE)

theme_estat <- function(...) {
  theme <- ggplot2:: theme_bw() +
    ggplot2:: theme(
      axis.title.y = ggplot2:: element_text(colour = "black",
                                            size = 12),
      axis.title.x = ggplot2:: element_text(colour = "black",
                                            size = 12),
      axis.text = ggplot2:: element_text(colour = "black", size
                                         = 9.5),
      panel.border = ggplot2:: element_blank (),
      axis.line = ggplot2:: element_line(colour = "black"),
      legend.position = "top",
      ...
    )
  return(
    list(
      theme ,
      scale_fill_manual(values = cores_estat),
      scale_colour_manual(values = cores_estat)
    )
  )
}
cores_estat <- c('#A11D21','#003366','#CC9900', '#663333','#FF6600','#CC9966','
#999966','#006606','#008091','#003366','#041835','#666666')

#___ Analise 1 ----
#Serie historica do numero de participantes nas olimpiadas de VERAO para cada sexo

#Criando objeto secundario para nao mexer no banco original
atletas <- athlete_events

#Criando colunas Ano e Estacao pela variavel Games
atletas <- atletas %>% mutate(Ano = as.numeric(str_sub(Games,1,4)),
                              Estacao = str_sub(Games,6),
                              Sexo = case_when(Sex == "F" ~ "Feminino",
                                               Sex == "M" ~ "Masculino",
                                               TRUE ~ Sex),
                              Medal = case_when(Medal == "Gold" ~ "Ouro",
                                                Medal == "Silver" ~ "Prata",
                                                Medal == "Bronze" ~ "Bronze"),
                              Estacao = case_when(Estacao == "Summer" ~ "Verão",
                                                  Estacao == "Winter" ~ "Inverno",
                                                  TRUE ~ Estacao))

#Organizando o banco para o grafico de linhas
#Filtrei pela estacao Verao, agrupei por Ano, Sexo e Nome do participante
#Agrupei pelo nome do participante para nao contar a mesma pessoa 2 vezes
linhas <- atletas %>% filter(Estacao == "Summer") %>% 
                      group_by(Ano, Sexo, Name) %>% 
                      summarise() %>%
                      distinct() %>% 
                      ungroup() %>% 
                      group_by(Ano, Sexo) %>% 
                      summarise(Participante = n())

#Grafico Linhas
ggplot(linhas) +
  aes(x = Ano , y = Participante , group = Sexo , colour = Sexo) +
  geom_line(size = 1) +
  geom_point(size = 2) +
  labs(x = "Ano da Olimpíada de Verão", y = "Número de Participantes") +
  theme_estat ()
ggsave("quest1linhas.png", width = 158, height = 93, units = "mm")

#___ Analise 2 -----
#Correlacao entre numero de medalhas e idade

#Banco com 2 colunas: qntdd de medalhas por idade (para correlacao dps)
medalhas1 <- atletas %>% group_by(Age) %>% 
                         filter(Medal != is.na(Medal), Age != is.na(Age))  %>%
                         summarise(quant = n())
             

#Banco com 2 colunas: tipo de medalha por idade (para fzr o Boxplot e correlacao dps)
medalhas2 <- atletas %>% group_by(Age, Medal) %>% 
             filter(Medal != is.na(Medal), Age != is.na(Age)) %>% 
             select(Medal, Age)


#Boxplot da Idade por tipo de Medalha
meda <- c("Ouro", "Prata","Bronze")
ggplot(medalhas2) +
  aes(x = factor(Medal, levels = meda) , y = Age) +
  geom_boxplot(fill = c("#A11D21"), width = 0.5) +
  stat_summary(
    fun = "mean", geom = "point", shape = 23, size = 3, fill = "white") +
  labs(x = "Tipo de Medalha", y = "Idade do Participante") +
  theme_estat ()
ggsave("quest2box.pdf", width = 158, height = 93, units = "mm")


#Banco com 3 colunas: Idade, tipo de medalha e qntdd de medalhas (Para grafico dispersao)
medalhas3 <- atletas %>% group_by(Age, Medal) %>% 
             summarise(quant = n()) %>%
             filter(Medal != is.na(Medal), Age != is.na(Age))  

#Dispersao do Numero de medalhas por idade
med <- c("Bronze", "Prata", "Ouro")
ggplot(medalhas3, aes(x=Age, y=quant)) + geom_point(aes(colour=factor(Medal, levels = med))) +
  scale_colour_manual(name="Tipo de Medalha", values = c('#A11D21','#003366','#CC9900'))+
  labs(x="Idade do Participante", y="Número de Medalhas")+
  theme_bw() +
  theme(axis.title.y=element_text(colour="black", size=12),
        axis.title.x = element_text(colour="black", size=12),
        axis.text = element_text(colour = "black", size=9.5),
        panel.border = element_blank(),
        axis.line = element_line(colour = "black")) +
  theme(legend.position="top") 
ggsave("quest2disp.png", width = 158, height = 93, units = "mm")


###Medidas Resumo Boxplot
summary(medalhas2$Age[medalhas2$Medal=="Bronze"])
sd(medalhas2$Age[medalhas2$Medal=="Bronze"])
summary(medalhas2$Age[medalhas2$Medal=="Prata"])
sd(medalhas2$Age[medalhas2$Medal=="Prata"])
summary(medalhas2$Age[medalhas2$Medal=="Ouro"])
sd(medalhas2$Age[medalhas2$Medal=="Ouro"])


###Testes correlacao
#Qualitativa X Quantitativa

#Hipoteses:
#H0: Nao ha diferença de idade entre os tipos de medalha
#H1: Pelo menos um tipo de medalha difere dos demais na idade.

#Teste de Normalidade por Lilliefors
LillieTest(medalhas2$Age)
#Nao a normal(p-valores <0,05)
#Dependente (pelos tipos de medalha serem dependentes)


#Banco com 3 colunas: Idade, tipo de medalha e qntdd de medalhas (Para correlacao de Friendman)
#Eh apenas o medalhas3 organizado no modelo do teste
medalhas4 <- data.frame(Age = rep(10:73,3),
                        Medal = c(rep("Ouro",64),rep("Prata",64),rep("Bronze",64))) %>% 
                        left_join(medalhas3, by = c("Age","Medal")) 
medalhas4[is.na(medalhas4$quant),]$quant <- 0

friedman.test(medalhas4$quant,medalhas4$Medal,medalhas4$Age)
#P-valor > 0,05 -> Aceita h0 - Nao tem diferença entre as medalhas


#Quantitativa X Quantitativa

#Teste Normalidade Shapiro-Wilk
LillieTest(medalhas1$quant)
#Normalidade de Idade já foi testada anteriomente

#Teste de Correlacao de Spearman
cor.test(medalhas1$quant, medalhas1$Age, method = "spearman")
#p-valor < 0,05 = rejeita hipotese nula de q não ha correlacao
#rho negativo = correlacao negativa moderada


#___ Analise 3 ----
#Quantidade de pódios por continente

#Juntando os dois bancos disponibilizados
completo <- left_join(atletas,country_definitions, by = "NOC")

#Função que diz o nome do continente pelo nome do país
completo$continente <- countrycode(sourcevar = completo$region, 
                                   origin = "country.name", 
                                   destination = "continent")

#Checando quais siglas (NOC) estão faltando
unique(completo$NOC[is.na(completo$continente == TRUE)])

#Consertando as que ficaram faltando acima
completo <- completo %>% mutate(continente = case_when(NOC == "SGP" ~ "Asia",
                                                       NOC == "BOL" ~ "Americas",
                                                       NOC == "FSM" ~ "Oceania",
                                                       NOC == "TUV" ~ "Oceania",
                                                       NOC == "KOS" ~ "Oceania",
                                                       #NOC == "RUS" ~ "Asia",
                                                       TRUE ~ continente))
#NA -> IOA ~ Individual Olympic Athletes, ROT ~ Refugee Olympic Team, UNK - Desconhecido

#Dataframe com número de pódios por continente
podio <- completo %>% group_by(continente) %>% 
                      filter(Medal != is.na(Medal), continente != is.na(continente))  %>%
                      summarise(quant = n())

#Conferindo se acima está certo
sum(podio$quant)
sum(completo$Medal != is.na(completo$Medal) & completo$continente != is.na(completo$continente), na.rm = T)

#Trocando nome Europe por Europa
podio <- podio %>% mutate(continente = case_when(continente == "Europe" ~ "Europa",
                                                 TRUE ~ continente))

#Gráfico Barras com porcentagem
podio <- podio %>% mutate(porc = round((quant*100)/sum(podio$quant),2))
porcentagens <- str_c(podio$porc , "%") %>% str_replace("\\.", ",")
legendas <- str_squish(str_c(podio$quant , " (", porcentagens , ")"))

ggplot(podio) +
  aes(x = fct_reorder(continente, quant, .desc=T), y = quant, label = legendas) +
  geom_bar(stat = "identity", fill = "#A11D21", width = 0.7) +
  geom_text(
    position = position_dodge(width = .9),
    vjust = -0.5, #hjust = .5,
    size = 3) +
  labs(x = "Continentes", y = "Frequência") +
  theme_estat ()
ggsave("quest3col.png", width = 158, height = 93, units = "mm")

#___ Analise 4 ----
#Distribuição dos IMCs por jogos de verão e inverno

#Criando coluna de IMC
atletas <- atletas %>% mutate(imc = as.numeric(Weight)/((as.numeric(Height)/100)^2))

#Gráfico boxplot
ggplot(atletas) +
  aes(x = Estacao , y = imc) +
  geom_boxplot(fill = c("#A11D21"), width = 0.5) +
  stat_summary(
    fun = "mean", geom = "point", shape = 23, size = 3, fill = "white") +
  labs(x = "Estação", y = "IMC dos participantes") +
  theme_estat ()
ggsave("boxplotimc.pdf", width = 158, height = 93, units = "mm")

#Medidas resumo
summary(atletas$imc[atletas$Estacao=="Verão"])
sd(atletas$imc[atletas$Estacao=="Verão"], na.rm = T)
summary(atletas$imc[atletas$Estacao=="Inverno"])
sd(atletas$imc[atletas$Estacao=="Inverno"], na.rm = T)

#Testando Normalidade

LillieTest(atletas$imc[atletas$Estacao=="Verão"])
LillieTest(atletas$imc[atletas$Estacao=="Inverno"])
#Nao normal(p-valores <0,05)

#Teste de Mann-Whitney
wilcox.test(atletas$imc[atletas$Estacao=="Verão"],atletas$imc[atletas$Estacao=="Inverno"])

#___ Analise 5 ----

#Filtrando os  atletas com seus respectivos pontos
peso1 <- atletas %>% filter(Medal != is.na(Medal)) %>% 
                    mutate(pesos = case_when(Medal == "Ouro" ~ 3,
                                             Medal == "Prata" ~ 2,
                                             Medal == "Bronze" ~ 1,
                                             TRUE ~ 0)) %>% 
                    group_by(Name) %>% 
                    summarise(n = sum(pesos)) %>%
                    arrange(desc(n)) %>% 
                    head(peso,n =5)

#Pontuação de todos os atletas por medalha
peso2 <- atletas %>% filter(Medal != is.na(Medal)) %>% 
                     mutate(pesos = 1) %>% 
                     group_by(Name, Medal) %>% 
                     summarise(n = sum(pesos))# %>%

#Juntando os dois bancos pra ter apenas os 5 melhores
#e suas pontuações por medalha
peso <- inner_join(peso1,peso2,by="Name")

#Adicionando linha de atleta faltante
peso[15,1] <- "Paavo Johannes Nurmi"
peso[15,2] <- 33
peso[15,3] <- "Bronze"
peso[15,4] <- 0


#Grafico do Top 5 pela pontuação de medalhas
legendas <- peso1$n
ggplot(peso1) +
  aes(x = fct_reorder(Name, n, .desc=T), y = n, label = legendas) +
  geom_bar(stat = "identity", fill = "#A11D21", width = 0.7) +
  geom_text(position = position_dodge(width = .9), 
            vjust = -0.5, hjust = .5, size = 3) +
  labs(x = "Atletas", y = "Frequência") +
  theme_estat() +
  scale_x_discrete(labels = function(x) str_wrap(x,width = 17))
ggsave("quest5top5total.pdf", width = 158, height = 98, units = "mm")


#Grafico do Top 5 pela pontuação de medalhas pelo tipo de medalha
legendas <- peso$n.y
meda <- c("Ouro", "Prata","Bronze")
ggplot(peso) +
  aes(x = fct_reorder(Name, n.x , .desc = T), y = n.y ,
      fill = factor(Medal, levels = meda) , label = legendas) +
  geom_col(position = position_dodge2(preserve = "single", padding = 0)) +
  geom_text(
    position = position_dodge(width = .9),
    vjust = -0.3, hjust = 0.5,
    size = 3) +
  labs(x = "Atletas", y = "Frequência") +
  scale_fill_manual(name="Medalha", values=c('#CC9900','#003366','#A11D21'))+
  theme_bw() +
  theme(axis.title.y=element_text(colour="black", size=12),
        axis.title.x = element_text(colour="black", size=12),
        axis.text = element_text(colour = "black", size=9.5),
        panel.border = element_blank(),
        axis.line = element_line(colour = "black")) +
  theme(legend.position="top") +
  scale_x_discrete(labels = function(x) str_wrap(x,width = 17))
ggsave("quest5top5dodge.pdf", width = 158, height = 100, units = "mm")

#___ Analise 6 ----
# Países com o menor número de participações entre todas as edições

#Juntando dois bancos
paises <- left_join(atletas,country_definitions, by = "NOC") 

#Nomeando países sem indicação no banco
#Calculando o top 5 paises com menos participações
paises <- paises %>% mutate(region = case_when(NOC == "TUV" ~ "Tuvalu",
                                               NOC == "SGP" ~ "Singapore",
                                               TRUE ~  region)) %>%
                     filter(region != is.na(region)) %>% 
                     group_by(region) %>%
                     mutate(n = 1) %>%
                     summarise(n = sum(n)) %>% 
                     arrange(n) %>% 
                     head(region,n = 5)

#Gráfico de colunas do top5 paises com menos participações
legendas <- paises$n
ggplot(paises) +
  aes(x = fct_reorder(region, n, .desc=F), y = n, label = legendas) +
  geom_bar(stat = "identity", fill = "#A11D21", width = 0.7) +
  geom_text(position = position_dodge(width = .9), 
            vjust = -0.5, hjust = .5, size = 3) +
  labs(x = "Países", y = "Frequência") +
  theme_estat()
ggsave("quest6top5paises.pdf", width = 158, height = 93, units = "mm")
